import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export const store = new Vuex.Store({
  state: {
    loadedMeetups: [
      {
        imageUrl: 'https://images.pexels.com/photos/683039/pexels-photo-683039.jpeg?auto=compress&cs=tinysrgb&h=650&w=940',
        id: 'kdgkyrjwlklnfhaifh25u92wrl',
        title: 'Jadwal Kopdar di Warung Kopi',
        date: new Date(),
        location: 'Sumarecon - Tangerang Selatan',
        description: 'Pertemuan atau kopdar ini dalam rangka acara halal bihalal, ajang silaturahim bersama rekan-rekan developer untuk kawasan Tangerang'
      },
      {
        imageUrl: 'https://images.pexels.com/photos/702251/pexels-photo-702251.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940',
        id: 'sjgs8wjw0sklsgj90836skfnk',
        title: 'Jadwal Kopdar di Kedai Kopi',
        date: new Date(),
        location: 'Margonda - Depok',
        description: 'Pertemuan atau kopdar ini dalam rangka membahas tentang perkembangan IT khususnya di bidang pengembangan aplikasi berbasis web modern'
      }
    ],
    user: {
      id: 'jtoeut93535535sfskf',
      registeredMeetups: ['sjgs8wjw0sklsgj90836skfnk']
    }
  },
  mutations: {
    createMeetup (state, payload) {
      state.loadedMeetups.push(payload)
    }
  },
  actions: {
    createMeetup ({commit}, payload) {
      const meetup = {
        title: payload.title,
        location: payload.location,
        imageUrl: payload.imageUrl,
        description: payload.description,
        date: payload.date,
        id: 'whriwhrkn92408204824j'
      }
      // Reach out  to firebase and store it
      commit('createMeetup', meetup)
    }
  },
  getters: {
    loadedMeetups (state) {
      return state.loadedMeetups.sort((meetupA, meetupB) => {
        return meetupA.date > meetupB.date
      })
    },
    featuredMeetups (state, getters) {
      return getters.loadedMeetups.slice(0, 5)
    },
    loadedMeetup (state) {
      return (meetupId) => {
        return state.loadedMeetups.find((meetup) => {
          return meetup.id === meetupId
        })
      }
    }
  }
})

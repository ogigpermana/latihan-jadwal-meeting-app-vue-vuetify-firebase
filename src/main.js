import Vue from 'vue'
import App from './App'
import router from './router'
import {
  Vuetify,
  VForm,
  VCard,
  VCarousel,
  VParallax,
  VTreeview,
  VAvatar,
  VImg,
  VDivider,
  VApp,
  VNavigationDrawer,
  VFooter,
  VList,
  VBtn,
  VIcon,
  VGrid,
  VToolbar,
  VTextField,
  VAutocomplete,
  VTooltip,
  VTextarea,
  VTimePicker,
  VDatePicker,
  VCheckbox,
  transitions
} from 'vuetify'
import '../node_modules/vuetify/src/stylus/app.styl'
import { store } from './store'
import DateFilter from './filters/date'
import * as VueGoogleMaps from 'vue2-google-maps'

Vue.use(Vuetify, {
  components: {
    VForm,
    VApp,
    VCard,
    VCarousel,
    VParallax,
    VTreeview,
    VAvatar,
    VImg,
    VDivider,
    VNavigationDrawer,
    VFooter,
    VList,
    VBtn,
    VIcon,
    VGrid,
    VToolbar,
    VTextField,
    VAutocomplete,
    VTooltip,
    VTextarea,
    VTimePicker,
    VDatePicker,
    VCheckbox,
    transitions
  },
  theme: {
    primary: '#4caf50',
    secondary: '#424242',
    accent: '#82B1FF',
    error: '#FF5252',
    info: '#2196F3',
    success: '#4CAF50',
    warning: '#FFC107',
    lgreen: '#4caf50'
  }
})

Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyBOrHeAUUVgPA6DNEv4I4nLXT8Brdzf7gw',
    libraries: 'places' // necessary for places input
  }
})

Vue.config.productionTip = false

Vue.filter('date', DateFilter)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
